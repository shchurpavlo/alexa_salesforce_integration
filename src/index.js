var errorMessage = 'Darn, there was a Salesforce problem, sorry';

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');
var nforce = require('nforce');
var _ = require('lodash');
var moment = require('moment-timezone');
var pluralize = require('pluralize');
var accountHandlers = require('./handlers/accountHandlers');
var credentials = require('./credentials/credentials');
var campaignHandlers = require('./handlers/campaignHandlers');
var productHandlers = require('./handlers/productHandlers');
var orderHandlers = require('./handlers/orderHandlers');
var contractHandlers = require('./handlers/contractHandlers');
var caseHandlers = require('./handlers/caseHandlers');
var taskHandlers = require('./handlers/taskHandlers');
var opportunityHandlers = require('./handlers/opportunityHandlers');
var leadHandlers = require('./handlers/leadHandlers');
var contactHandlers = require('./handlers/contactHandlers');

/**
 * Salesforce is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var Salesforce = function () {
    AlexaSkill.call(this, credentials.data.APP_ID);
};

var org = nforce.createConnection({
  clientId: credentials.data.CLIENT_ID,
  clientSecret: credentials.data.CLIENT_SECRET,
  redirectUri: credentials.data.CALLBACK_URL,
  mode: 'single'
});

// Extend AlexaSkill
Salesforce.prototype = Object.create(AlexaSkill.prototype);
Salesforce.prototype.constructor = Salesforce;

Salesforce.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("Salesforce onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

Salesforce.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("Salesforce onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
};

/**
 * Overridden to show that a subclass can override this function to teardown session state.
 */
Salesforce.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("Salesforce onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

Salesforce.prototype.intentHandlers = {

  /************************************
		The logic for Account // intents
	************************************/

  //check for accounts
  AccountsIntent: function (intent, session, response) {
    accountHandlers.data.handleAccountsRequest_Get(response);
  },

  // check for account by specified name
  AccountNameIntent: function (intent, session, response) {
    accountHandlers.data.handleAccountNameRequest_Get(intent, response);
  },

  // check for any new accounts
  NewAccountsIntent: function (intent, session, response){
    accountHandlers.data.handleNewAccountsRequest_Get(response);
  },

  /************************************
		The logic for Contact // intents
	************************************/

   //check for contacts
  ContactsIntent: function (intent, session, response) {
    contactHandlers.data.handleContactsRequest_Get(response);
  },

  // check for any new contacts
  NewContactsIntent: function (intent, session, response) {
    contactHandlers.data.handleNewContactsRequest_Get(response);
  },

  // check for contact by specified name
  ContactNameIntent: function (intent, session, response){
    contactHandlers.data.handleContactNameRequest_Get(intent, response); 
  },

  /************************************
		The logic for Lead // intents
  ************************************/
  
 //check for leads
  LeadsIntent: function (intent, session, response) {
    leadHandlers.data.handleLeadsRequest_Get(response);
 },

  // check for any new leads
  NewLeadsIntent: function (intent, session, response) {
    leadHandlers.data.handleNewLeadsRequest_Get(response);
 },

 //  check for lead by specified name
  LeadNameIntent_Get: function (intent, session, response) {
    leadHandlers.data.handleLeadNameRequest_Get(intent, response); 
 },

  OpportunitiesIntent: function(intent, session, response){
    opportunityHandlers.data.handleAllOpportunitiesRequest_Get(response);
  },

  // check for any new opportunities  
  NewOpportunitiesIntent: function (intent, session, response) {
    opportunityHandlers.data.handleNewOpportunitiesRequest_Get(response);
  },

  // check the status of an apportunity by name
  OpportunityNameIntent: function (intent, session, response) {
    opportunityHandlers.data.handleOpportunityNameRequest(intent, response);
  },

  // start the new lead creation process
  LeadStartIntent: function (intent, session, response) {
      handleLeadStartRequest(session, response);
  },

  // get the name and create the lead
  LeadCompanyIntent: function (intent, session, response) {
      handleLeadCompanyIntent(intent, session, response);
  },

  // check my calendar
  MyCalendarIntent: function (intent, session, response) {
      handleMyCalendarRequest(response);
  },

  // check for any tasks  
  TasksIntent: function (intent, session, response){
    taskHandlers.data.handleAllTasksRequest_Get(response);
  },

  // check for any new tasks
  NewTasksIntent: function (intent, session, response) {
    taskHandlers.data.handleNewTasksRequest_Get(response);
  },

  TaskNameIntent_Get: function(intent, session, response){
    taskHandlers.data.handleTaskSubjectRequest_Get(intent, response);
  },

  // check for any cases  
  CasesIntent: function(intent, session, response) {
    caseHandlers.data.handleAllCasesRequest_Get(response); 
  },

  // check for tasks created today
  NewCasesIntent: function(intent, session, response) {
    caseHandlers.data.handleNewCasesRequest_Get(response); 
  },

  // check for task by reason  
  CaseReasonIntent_Get: function(intent, session, response) {
    caseHandlers.data.handleCaseReasonRequest_Get(intent, response);
  },

  // check for any contracts  
  ContractsIntent: function(intent, session, response){
    contractHandlers.data.handleAllContractsRequest_Get(response);
  },

  // check for contracts created today
  NewContractsIntent: function(intent, session, response) {
    contractHandlers.data.handleNewContractsRequest_Get(response);
  },

  // check for contract by status
  ContractStatusIntent_Get: function(intent, session, response) {
    contractHandlers.data.handleContractStatusRequest_Get(intent, response);
  },

  // check for any orders  
  OrdersIntent: function(intent, session, response) {
    orderHandlers.data.handleAllOrdersRequest_Get(response);
  },

  // check for orders created today
  NewOrdersIntent: function(intent, session, response) {
    orderHandlers.data.handleNewOrdersRequest_Get(response);
  },

  // check for order by status
  OrderStatusIntent_Get: function(intent, session, response) {
    orderHandlers.data.handleOrderStatusRequest_Get(intent, response);
  },

  // check for any products
  ProductsIntent: function(intent, session, response) {
    productHandlers.data.handleAllProductsRequest_Get(response);
  },

  // check for products created today
  NewProductsIntent: function(intent, session, response) {
    productHandlers.data.handleNewProductsRequest_Get(response);
  },

  // check for product by name
  ProductNameIntent: function(intent, session, response) {
    productHandlers.data.handleProductNameRequest_Get(intent, response);
  },

  // check for any campaigns
  CampaignsIntent: function(intent, session, response) {
    campaignHandlers.data.handleAllCampaignsRequest_Get(response);
  },

  // check for sampaigns created today
  NewCampaignsIntent: function(intent, session, response) {
    campaignHandlers.data.handleNewCampaignsRequest_Get(response);
  },

  // check for campaigns by name
  CampaignTypeIntent: function(intent, session, response) {
    campaignHandlers.data.handleCampaignTypeRequest_Get(intent, response);
  },

    // help with 'Salesforce'
    HelpIntent: function (intent, session, response) {
      response.ask("You can ask Salesforce to check for any new leads, your calendar for today, the status of a specific opportunity or to create a new lead, or, you can say exit... What can I help you with?");
  }
};

// start a new session to create a lead
function handleLeadStartRequest(session, response) {
  var speechOutput = "OK, let's create a new lead., What is the person's first and last name?";
  response.ask(speechOutput);
}

// continue the session, collect the person's name
function handleLeadNameIntent(intent, session, response) {
  var speechOutput = "Got it. the name is, " + intent.slots.Name.value + "., What is the company name?";
  session.attributes.name = intent.slots.Name.value;
  response.ask(speechOutput);
}

// collect the company name and create the actual lead
function handleLeadCompanyIntent(intent, session, response) {
  var speechOutput = "Bingo! I created a new lead for  "
    + session.attributes.name + " with the company name " + intent.slots.Company.value;
  var names = session.attributes.name.split(' ');
  var obj = nforce.createSObject('Lead');
  obj.set('FirstName', names[0]);
  obj.set('LastName', names[1]);
  obj.set('Company', intent.slots.Company.value);

  org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
    return org.insert({ sobject: obj })
  }).then(function(results) {
    if (results.success) {
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    } else {
      speechOutput = errorMessage;
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }
  }).error(function(err) {
    var errorOutput = errorMessage;
    response.tell(errorOutput, "Salesforce", errorOutput);
  });
}

// find any calendar events for today
function handleMyCalendarRequest(response) {
  var query = 'select id, StartDateTime, Subject, Who.Name from Event where startdatetime = TODAY order by StartDateTime';
  // auth and run query
  org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
    return org.query({ query: query })
  }).then(function(results) {
    var speechOutput = 'You have  ' + results.records.length + ' ' + pluralize('event', results.records.length) + ' for today, ';
    _.forEach(results.records, function(rec) {
      speechOutput += 'At ' + moment(rec.get('StartDateTime')).tz('America/Los_Angeles').format('h:m a') + ', ' + rec.get('Subject');
      if (rec.get('Who')) speechOutput += ', with  ' + rec.get('Who').Name;
      speechOutput += ', ';
    });
    response.tellWithCard(speechOutput, "Salesforce", speechOutput);
  }).error(function(err) {
    var errorOutput = errorMessage;
    response.tell(errorOutput, "Salesforce", errorOutput);
  });
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the Salesforce skill.
    var salesforce = new Salesforce();
    salesforce.execute(event, context);
};