var errorMessage = 'Darn, there was a Salesforce problem, sorry';

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('AlexaSkill');
var nforce = require('nforce');
var _ = require('lodash');
var moment = require('moment-timezone');
var pluralize = require('pluralize');
var credentials = require('credentials/credentials');
var campaignHandlers = require('handlers/campaignHandlers');

/**
 * Salesforce is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var Salesforce = function () {
    AlexaSkill.call(this, credentials.data.APP_ID);
};

var org = nforce.createConnection({
  clientId: credentials.data.CLIENT_ID,
  clientSecret: credentials.data.CLIENT_SECRET,
  redirectUri: credentials.data.CALLBACK_URL,
  mode: 'single'
});

var handlers = {
/**
 * @returns all opportunities
 */
handleAllOpportunitiesRequest_Get : function(response) {
    var query = 'Select Name from Opportunity';
    // auth and run query
    org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
      return org.query({ query: query })
    }).then(function(results) {
      speechOutput = 'Sorry, you do not have any opportunities.'
      var recs = results.records;
      if (recs.length > 0) {
        speechOutput = 'You have ' + recs.length + ' ' + pluralize('opportunity', recs.length) + ', ';
        for (i=0; i < recs.length; i++){
          speechOutput +=  i+1 + ', ' + recs[i].get('Name') + ', ';
          if (i === recs.length-2) speechOutput += ' and ';
        }
        speechOutput += ', Go get them tiger!';
      }
      // Create speech output
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }).error(function(err) {
      var errorOutput = errorMessage;
      response.tell(errorOutput, "Salesforce", errorOutput);
    });
  },
  
  /**
 * @returns all opportunities created today
 */
handleNewOpportunitiesRequest_Get : function(response) {
    var query = 'Select Name from Opportunity where CreatedDate = TODAY';
    // auth and run query
    org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
      return org.query({ query: query })
    }).then(function(results) {
      speechOutput = 'Sorry, you do not have any new opportunties for today.'
      var recs = results.records;
      if (recs.length > 0) {
        speechOutput = 'You have ' + recs.length + ' new ' + pluralize('lead', recs.length) + ', ';
        for (i=0; i < recs.length; i++){
          speechOutput +=  i+1 + ', ' + recs[i].get('Name') + ' from ' + recs[i].get('Company') + ', ';
          if (i === recs.length-2) speechOutput += ' and ';
        }
        speechOutput += ', Go get them tiger!';
      }
      // Create speech output
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }).error(function(err) {
      var errorOutput = errorMessage;
      response.tell(errorOutput, "Salesforce", errorOutput);
    });
  },

/**
 * @returns opportunity by name
 */
handleOpportunityNameRequest : function(intent, response) {
    var opportunityName = intent.slots.OpportunityName.value;
    var query = "Select Name, StageName, Probability, Amount from Opportunity where Name = '" + opportunityName + "'";
    // auth and run query
    org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
      return org.query({ query: query })
    }).then(function(results) {
      var speechOutput = 'Sorry, I could not find an Opportunity named, ' + opportunityName;
      if (results.records.length > 0) {
        var opp = results.records[0];
        speechOutput = 'I found Opportunity ' + opportunityName + ' for $' + opp.get('Amount')
          + ', the stage is ' + opp.get('StageName') + ' and the probability is '
          + opp.get('Probability') + '%';
      }
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }).error(function(err) {
      var errorOutput = errorMessage;
      response.tell(errorOutput, "Salesforce", errorOutput);
    });
  }
}

exports.data = handlers;