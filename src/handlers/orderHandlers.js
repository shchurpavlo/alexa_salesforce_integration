var errorMessage = 'Darn, there was a Salesforce problem, sorry';

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('AlexaSkill');
var nforce = require('nforce');
var _ = require('lodash');
var moment = require('moment-timezone');
var pluralize = require('pluralize');
var credentials = require('credentials/credentials');
var campaignHandlers = require('handlers/campaignHandlers');

/**
 * Salesforce is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var Salesforce = function () {
    AlexaSkill.call(this, credentials.data.APP_ID);
};

var org = nforce.createConnection({
  clientId: credentials.data.CLIENT_ID,
  clientSecret: credentials.data.CLIENT_SECRET,
  redirectUri: credentials.data.CALLBACK_URL,
  mode: 'single'
});

var handlers = {

/**
 * @returns all orders
 */
handleAllOrdersRequest_Get : function(response) {
    var query = 'Select Status from Order';
    // auth and run query
    org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
      return org.query({ query: query })
    }).then(function(results) {
      speechOutput = 'Sorry, you do not have any orders.'
      var recs = results.records;
      if (recs.length > 0) {
        speechOutput = 'You have ' + recs.length + ' ' + pluralize('order', recs.length) + ', ';
        for (i = 0; i < recs.length; i++){
          speechOutput +=  i + 1 + ', ' + recs[i].get('Status');
          if (i === recs.length - 2) speechOutput += ' and ';
        }
        speechOutput += ', Go get them tiger!';
      }
      // Create speech output
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }).error(function(err) {
      var errorOutput = errorMessage;
      response.tell(errorOutput, "Salesforce", errorOutput);
    });
  },
  
  /**
   * @returns all orders created today
   */
  handleNewOrdersRequest_Get : function(response) {
    var query = 'Select Status FROM Order where CreatedDate = TODAY';
    // auth and run query
    org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
      return org.query({ query: query })
    }).then(function(results) {
      speechOutput = 'Sorry, you do not have any new orders for today.'
      var recs = results.records;
      if (recs.length > 0) {
        speechOutput = 'You have ' + recs.length + ' new ' + pluralize('Order', recs.length);
        for (i = 0; i < recs.length; i++){
          speechOutput +=  i + 1 + ', ' + recs[i].get('name');
          if (i === recs.length-2) speechOutput += ' and ';
        }
        speechOutput += ', Go get them tiger!';
      }
      // Create speech output
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }).error(function(err) {
      var errorOutput = errorMessage;
      response.tell(errorOutput, "Salesforce", errorOutput);
    });
  },
  
  /**
   * @returns order by status
   */
  handleNewOrdersRequest_Get : function(intent, response) {
    var orderStatus = intent.slots.OrderStatus.value;
    var query = "Select Status from Order where Status = '" + orderStatus + "'";
    // auth and run query
    org.authenticate({ username: credentials.data.USERNAME, password: credentials.data.PASSWORD }).then(function(){
      return org.query({ query: query })
    }).then(function(results) {
      var speechOutput = 'Sorry, I could not find a Order with status, ' + orderStatus;
      if (results.records.length > 0) {
        var opp = results.records[0];
        speechOutput = 'I found Order with status ' + orderStatus;
      }
      response.tellWithCard(speechOutput, "Salesforce", speechOutput);
    }).error(function(err) {
      var errorOutput = errorMessage;
      response.tell(errorOutput, "Salesforce", errorOutput);
    });  
  }
}

exports.data = handlers;